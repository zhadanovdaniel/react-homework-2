
import React, { useState, useEffect } from "react";
import "./HomeHeader.scss";

export default function HomeHeader({counter, isAdded}) {
  const [cartCount, setCartCount] = useState(0);
  const [favoritesCount, setFavoritesCount] = useState(0);

  useEffect(() => {
    // Отримання кількості товарів у кошику з localStorage
    const cartItems = JSON.parse(localStorage.getItem("addedToCart")) || [];
    setCartCount(cartItems.length);
    
    // Отримання кількості обраних товарів з localStorage
    // const favorites = JSON.parse(localStorage.getItem("favorites")) || [];
    // setFavoritesCount(favorites.length);
  }, []);


  useEffect(() => {
    // Отримання кількості товарів у кошику з localStorage
    const cartItems = JSON.parse(localStorage.getItem("addedToCart")) || [];
    setCartCount(cartItems.length);
    
    // Отримання кількості обраних товарів з localStorage
    // const favorites = JSON.parse(localStorage.getItem("favorites")) || [];
    // setFavoritesCount(favorites.length);
    console.log('header :>> ');
  }, [isAdded, !isAdded]);

  return (
    <>
      <header>
        <nav>
          <div className="logo">
            <a href="/">Магазин футболок</a>
          </div>
          <ul className="menu">
            <li>
              <a href="/">Головна</a>
            </li>
            <li>
              <a href="/products">Продукти</a>
            </li>
            <li>
              <a href="/contact">Контакти</a>
            </li>
          </ul>
          <div className="icons">
            <a href="/cart">
              <img src="https://png2.cleanpng.com/sh/b1c05cb6790f582e4c3c429f7270b88e/L0KzQYm3VsI0N6J6g5H0aYP2gLBuTfFxeJ1qRdt5aHBxdX6ATgBtfaQygeJxb37oPYm0hPl0a5D6huZ8LXHxdH7ojPxwf5JzReVxb4Dsfri0gBFzfF46eqRuNETmc7LqVfViPF8ATqk9M0G8RoK8Usk4PWkASqYCMEaxgLBu/kisspng-apple-iphone-7-plus-iphone-8-discounts-and-allowan-shoping-cart-5b2e44ccac5ea4.967431961529758924706.png" alt="Кошик" />
              <span>{cartCount}</span>
            </a>
            <a href="/wishlist">
              <img src="https://png2.cleanpng.com/sh/8537f61b32846d84fbabeff6b73bb658/L0KzQYq3V8A1N5NufpH0aYP2gLBuTgBweqVmet5uLX7ohMj2kvsub6NmiNpyY4Owc73wkL1ieqUygd9qZ3WwhrbqlP9zNZh3ReVBZz32hLL5TgRzaZ94iNN7ZX73PbL0kL1xdpgye95ycHH1hH7tkvVmNZV0j9D1bz24dILsgck6OGVpTtVuMD62QISBU8M1PmI6TqQ7OUC4R4e6UccyNqFzf3==/kisspng-portable-network-graphics-clip-art-image-vector-gr-svg-star-transparent-amp-png-clipart-free-downlo-5d1ea9904d6ce0.3038334615622905763171.png" alt="Обране" />
              {/* <span>{favoritesCount}</span> */}
              <span>{counter}</span>
            </a>
          </div>
        </nav>
      </header>
    </>
  );
}

import HomeHeader from "./HomeHeader";
import HomeBody from "./HomeBody";
import axios from "axios";
import React, {  useState, useEffect } from "react";
import CardWrapper from "../CardWrapper/CardWrapper";

//має бути три масиви для фейворіта для кошика і продактс як стейти
export default function HomePage() {
  const [products, setProducts] = useState([]);
  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(true);
  const [counter, setCounter] = useState(0)// переробити на масив артикулів
 
   const [isAdded, setIsAdded]= useState(false);

  useEffect(() => {
    axios
      .get("/products.json")
      .then((response) => {
        setProducts(response.data);
        setLoading(false);
      })
      .catch((error) => {
        setError("Помилка завантаження даних про товари.");
        setLoading(false);
      });
  }, []);

  if (loading) {
    return <div>Завантаження...</div>;
  }
  if (error) {
    return <div>{error}</div>;
  }
  
  return (
    <>
      <div className="HomePage">
     
      
      <HomeHeader counter ={counter} isAdded = {isAdded}/>

        <HomeBody>
          <CardWrapper products={products} setCount={setCounter}  setIsAdded={setIsAdded}/>
        </HomeBody>

      
      </div>
    </>
  );
}






import React, { useState, useEffect } from "react";
import Card from "../Card/Card";
import "./CardWrapper.scss";


import Modal from "../Modal/Modal";
import ModalWrapper from "../Modal/ModalWrapper";
import ModalHeader from "../Modal/ModalHeader";
import ModalFooter from "../Modal/ModalFooter";
import ModalBody from "../Modal/ModalBody";
import ModalClose from "../Modal/ModalClose";


export default function CardWrapper({ products, setCount, setIsAdded}) {
  const [activeStarsCount, setActiveStarsCount] = useState(0);
  



  




  useEffect(() => {
    // Отримання обраних товарів з localStorage
    const favorites = JSON.parse(localStorage.getItem("favorites")) || [];
    setActiveStarsCount(favorites.length);
  }, []);


  const [isModalOpen, setIsModalOpen] = useState(false); // Стейт для первой модалки
  const [activeArticle, setActiveArticle] = useState(null)
  
  const openModal = () => {
    setIsModalOpen(true);
  };
  const closeModal = () => {
    setIsModalOpen(false);
     };



     const [activeCartCount, setActiveCartCount] = useState(0);


     useEffect(() => {
      // Отримання обраних товарів з localStorage
      const addedToCart = JSON.parse(localStorage.getItem("addedToCart")) || [];
      setActiveCartCount(addedToCart.length);
    }, []);
  


    const handleCartClick = (productArticle) => {
      // Збереження стану обраного товару в localStorage
      const addedToCart = JSON.parse(localStorage.getItem("addedToCart")) || [];
      
        if (!addedToCart.includes(activeArticle)) {
        localStorage.setItem("addedToCart", JSON.stringify([...addedToCart, productArticle]));
        // setCount(prevCount => prevCount + 1 )
      } else {
        localStorage.setItem("addedToCart", JSON.stringify(addedToCart.filter(article => article !== productArticle)));
        // setCount(prevCount => prevCount - 1 )
      }
    };
  
  
  
  

  return (
    <>
      <h2>Футболки у асортименті:</h2>
      <div className="cardWrapper">
        {products.map((product) => (
          <Card
            key={product.article}
            src={product.image}
            alt={product.name}
            productName={product.name}
            productPrice={product.price}
            productArticle={product.article}
            productColor={product.color}
            ModalOpen={openModal}
            setActiveStarsCount={setActiveStarsCount} // Передача функції в компонент Card
            setActiveCartCount={setActiveCartCount}
            setCount={setCount}
            setActiveArticle = {setActiveArticle}
            // handleCartClick={handleCartClick}
          />
        ))}
      </div>
      <div>Загальна кількість активних зірочок на всіх карточках: {activeStarsCount}</div>
      <div>Загальна кількість добалених до кошика: {activeCartCount}</div>





      <Modal isOpen={isModalOpen}>
        <ModalWrapper>
          <ModalHeader>
            <ModalClose onClick={closeModal} />
            
          </ModalHeader>

          <ModalBody>
            <h1>Add to cart</h1>
            <p>
              By clicking the “YES, ADD TO CART” button, product will be added <br />
              to cart
            </p>
          </ModalBody>
          <ModalFooter
            firstText={ "NO, CANCEL"  }
            secondaryText={true ? "YES, ADD TO CART" : "Delete"}
            // secondaryText={"YES, ADD TO CART"  }
            firstClick={closeModal}
            secondaryClick={()=>{handleCartClick(activeArticle); setIsAdded((prev)=>!prev); closeModal()}}
          ></ModalFooter>
        </ModalWrapper>
      </Modal>
    </>
  );
}

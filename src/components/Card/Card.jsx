
import React, { useState, useEffect } from "react";
import "./Card.scss";

export default function Card(props) {
  const { src, alt, productName, productPrice, productArticle, productColor, setActiveStarsCount, ModalOpen, setCount, setActiveCartCount, setActiveArticle} = props;
  const [fav, setFav] = useState(false);
  
  





  useEffect(() => {
    // Завантаження стану обраного товару з localStorage
    const favorites = JSON.parse(localStorage.getItem("favorites")) || [];
    setFav(favorites.includes(productArticle));
  }, [productArticle]); // Перезавантаження стану, якщо змінюється артикул товару

  const handleStarClick = () => {
    const updatedFav = !fav;
    setFav(updatedFav);
    setActiveStarsCount(prevCount => prevCount + (updatedFav ? 1 : -1));
    
    
    // Збереження стану обраного товару в localStorage
    const favorites = JSON.parse(localStorage.getItem("favorites")) || [];
    
      if (updatedFav) {
      localStorage.setItem("favorites", JSON.stringify([...favorites, productArticle]));
      setCount(prevCount => prevCount + 1 )
    } else {
      localStorage.setItem("favorites", JSON.stringify(favorites.filter(article => article !== productArticle)));
      setCount(prevCount => prevCount - 1 )
    }
  };







  
  const [cart, setCart] = useState(false);
  useEffect(()=>{
    const addedToCart = JSON.parse(localStorage.getItem("addedToCart")) || [];
    setCart(addedToCart.includes(productArticle));
  },[productArticle])

  // const handleCartClick = () => {
  //   const updatedCart = !cart;
  //   setCart(updatedCart);
  //   setActiveCartCount(prevCount => prevCount + (updatedCart ? 1 : -1));
  
    
  //   // Збереження стану обраного товару в localStorage
  //   const addedToCart = JSON.parse(localStorage.getItem("addedToCart")) || [];
    
  //     if (updatedCart) {
  //     localStorage.setItem("addedToCart", JSON.stringify([...addedToCart, productArticle]));
  //     // setCount(prevCount => prevCount + 1 )
  //   } else {
  //     localStorage.setItem("addedToCart", JSON.stringify(addedToCart.filter(article => article !== productArticle)));
  //     // setCount(prevCount => prevCount - 1 )
  //   }
  // };









  return (
    <div className="card">
      <img src={src} alt={alt} />
      <div>{productName}</div>
      <div>{productPrice} грн</div>
      <div>Артикул: {productArticle}</div>
      <div>Колір: {productColor}</div>
      <button className={'shoping_cart'} onClick={()=>{ModalOpen(); setActiveArticle(productArticle)}}>Add to cart</button>
      <div className={`star ${fav ? 'active' : ''}`} onClick={handleStarClick}></div>
      
    </div>
  );
}
